package cz.ucl.br.model.types;

public enum ServiceType {
    Trip, Spa, Other
}
