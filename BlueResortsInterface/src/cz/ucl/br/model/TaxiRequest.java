package cz.ucl.br.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@SuppressWarnings("serial")
@Entity
public class TaxiRequest implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "fromPlace")
    private String from;

    @Column(name = "toPlace")
    private String to;

    private String location;

    private Double distance;

    private String clientContact;

    private String clientName;

    @OneToMany(mappedBy = "request", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<TaxiResponse> responses;

    @OneToOne
    private TaxiResponse selectedResponse;

    public Long getId() {
        return id;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public Double getDistance() {
        return distance;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getClientContact() {
        return clientContact;
    }

    public void setClientContact(String clientContact) {
        this.clientContact = clientContact;
    }

    public Set<TaxiResponse> getResponses() {
        return responses;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public TaxiResponse getSelectedResponse() {
        return selectedResponse;
    }

    public void setSelectedResponse(TaxiResponse selectedResponse) {
        this.selectedResponse = selectedResponse;
    }


}
