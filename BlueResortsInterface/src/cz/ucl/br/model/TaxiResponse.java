package cz.ucl.br.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class TaxiResponse implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private TaxiRequest request;

    private String taxiName;
    private String taxiDescription;
    private Double price;

    public Long getId() {
        return id;
    }

    public String getTaxiName() {
        return taxiName;
    }

    public String getTaxiDescription() {
        return taxiDescription;
    }

    public Double getPrice() {
        return price;
    }

    public TaxiRequest getRequest() {
        return request;
    }

    public void setRequest(TaxiRequest request) {
        this.request = request;
    }

    public void setTaxiName(String taxiName) {
        this.taxiName = taxiName;
    }

    public void setTaxiDescription(String taxiDescription) {
        this.taxiDescription = taxiDescription;
    }

    public void setPrice(Double price) {
        this.price = price;
    }


}
