package cz.ucl.br.ejb;

import java.util.List;

import cz.ucl.br.model.Hotel;
import cz.ucl.br.model.Room;

public interface HotelManager {
    List<Hotel> listHotels();

    Hotel selectHotel(Hotel hotel);

    //Potřebuje znát stav (vybraný hotel)
    void addRoom(Room room);

    void changeRoom(Room room);

    void removeRoom(Room room);

    void closeHotel();
}
