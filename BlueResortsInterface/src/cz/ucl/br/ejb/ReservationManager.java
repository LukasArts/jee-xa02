package cz.ucl.br.ejb;

import java.util.Date;
import java.util.List;

import cz.ucl.br.model.Hotel;

public interface ReservationManager {
    List<String> getAllLocations();

    List<Hotel> findHotels(String location);

    String createReservation(String hotelName, String customerName,
                             int bedCount, Date from, Date to)
            throws NoRoomAvailableException;

    void cancelReservation(String reservationCode) throws NoCancellationException;
}
