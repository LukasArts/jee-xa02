package cz.ucl.br.ejb;

import javax.ejb.Remote;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import cz.ucl.br.model.TaxiRequest;

@Remote
public interface TaxiRequestManager {

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    Long requestTaxi(TaxiRequest request);

}
