package cz.ucl.br.ejb;

@SuppressWarnings("serial")
public class NoRoomAvailableException extends Exception {
    private Integer maxAvailableCapacity;

    public Integer getMaxAvailableCapacity() {
        return maxAvailableCapacity;
    }

    public void setMaxAvailableCapacity(Integer maxAvailableCapacity) {
        this.maxAvailableCapacity = maxAvailableCapacity;
    }


}
