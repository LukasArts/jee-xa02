package cz.ucl.br.ejb;


public interface ApplicationLog {
    void logAccess(String action, String object);
}
