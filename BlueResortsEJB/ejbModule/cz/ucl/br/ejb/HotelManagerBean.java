package cz.ucl.br.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import cz.ucl.br.model.Hotel;
import cz.ucl.br.model.Room;

@Local(HotelManager.class)
@Stateful
@Interceptors({ApplicationLogInterceptor.class})
public class HotelManagerBean implements HotelManager {
    @EJB
    ApplicationLog appLog;

    private Hotel selectedHotel;

    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Hotel> listHotels() {
        return em.createQuery("SELECT h FROM Hotel h").getResultList();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Hotel selectHotel(Hotel hotel) {
        selectedHotel = em.merge(hotel);
        selectedHotel.getRooms().size();
        selectedHotel.getServices().size();
        return selectedHotel;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void addRoom(Room room) {
        appLog.logAccess("Add a room", room.toString());
        room.setHotel(selectedHotel);
        em.persist(room);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void changeRoom(Room room) {
        em.merge(room);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void removeRoom(Room room) {
        Room mergedRoom = em.merge(room);
        selectedHotel.getRooms().remove(mergedRoom);
        em.remove(mergedRoom);
    }

    @Remove
    public void closeHotel() {
        selectedHotel = null;
    }

}
