package cz.ucl.br.ejb;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.mail.MessagingException;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.masukomi.aspirin.core.MailQue;
import org.masukomi.aspirin.core.SimpleMimeMessageGenerator;

import cz.ucl.br.model.TaxiRequest;
import cz.ucl.br.model.TaxiResponse;

@Stateless
@Interceptors({ApplicationLogInterceptor.class})
public class TaxiRequestManagerBean implements TaxiRequestManager {
    private static Logger log = Logger.getLogger(TaxiRequestManagerBean.class
            .getName());

    @PersistenceContext
    private EntityManager em;

    @Resource
    private TimerService timerService;

    @Resource(mappedName = "java:/jms/topic/BRTaxiRequestTopic")
    private Destination requestTopic;

    @Resource(mappedName = "java:/jms/queue/BRTaxiResponseQueue")
    private Destination responseQueue;

    @Resource(mappedName = "java:jboss/DefaultJMSConnectionFactory")
    private ConnectionFactory cf;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Long requestTaxi(TaxiRequest request) {
        em.persist(request);

        try {
            Connection con = cf.createConnection();
            Session sess = con.createSession(false, Session.AUTO_ACKNOWLEDGE);

            MessageProducer sender = sess.createProducer(requestTopic);

            Message msg = sess.createObjectMessage(request);
            msg.setJMSReplyTo(responseQueue);
            sender.send(msg);

            sender.close();
            sess.close();
            con.close();

            log.info("Taxi request " + request.getId() + " sent.");
        } catch (JMSException e) {
            log.severe("Taxi request sending failed: " + e.getMessage());
            throw new RuntimeException(e);
        }

        timerService.createTimer(60 * 1000, request.getId());

        return request.getId();
    }

    @Timeout
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void aggregateResponses(Timer timer) {
        Long requestId = (Long) timer.getInfo();
        log.info("Aggregating responses for request " + requestId);

        TaxiRequest request = em.find(TaxiRequest.class,
                requestId);

        // najdi nejlepší odpověď
        TaxiResponse selectedResponse = null;
        for (TaxiResponse resp : request.getResponses()) {
            if ((selectedResponse == null)
                    || (selectedResponse.getPrice() > resp.getPrice()))
                selectedResponse = resp;
        }
        request.setSelectedResponse(selectedResponse);

        try {
            MimeMessage email = SimpleMimeMessageGenerator.getNewMimeMessage();
            email.setRecipient(RecipientType.TO, new InternetAddress(request
                    .getClientContact()));
            email.setFrom(new InternetAddress("noreply-jee-robot@unicorncollege.cz"));
            email.setSubject("Taxi response");

            StringWriter mailText = new StringWriter();
            PrintWriter mailTextWriter = new PrintWriter(mailText);

            if (selectedResponse != null) {
                log.info("Found best response for request " + requestId);

                mailTextWriter
                        .println("A taxi will be waiting at your requested pick-up location.");
                mailTextWriter.println();
                mailTextWriter
                        .println("Pick-up location: " + request.getFrom());
                mailTextWriter.println("Destination: " + request.getTo());
                mailTextWriter.println("Taxi description: "
                        + selectedResponse.getTaxiDescription());
                mailTextWriter.println("Driver's name: "
                        + selectedResponse.getTaxiName());
                mailTextWriter.println();
                mailTextWriter
                        .println("Thank you for using the Blue Resorts Shuttle service.");
                mailTextWriter.println();
                mailTextWriter.println("Blue Resorts");
            } else {
                log.info("Found no response for request " + requestId);

                mailTextWriter
                        .println("Unfortunately we were unable to find a taxi for you. Please contact the front desk if you need further assistance.");
                mailTextWriter.println();
                mailTextWriter
                        .println("Pick-up location: " + request.getFrom());
                mailTextWriter.println("Destination: " + request.getTo());
                mailTextWriter.println();
                mailTextWriter
                        .println("Thank you for using the Blue Resorts Shuttle service.");
                mailTextWriter.println();
                mailTextWriter.println("Blue Resorts");
            }
            mailTextWriter.close();

            email.setText(mailText.toString());
            MailQue queue = new MailQue();
            queue.queMail(email);
            log
                    .info("Sent email notification to customer ("
                            + request.getClientContact() + ") for request "
                            + requestId);
        } catch (AddressException e1) {
            log.severe("E-mail sending failed for taxi request " + requestId
                    + ", wrong customer e-mail '" + request.getClientContact()
                    + "'");
        } catch (MessagingException e1) {
            log.severe("E-mail sending failed for taxi request " + requestId);
        }

    }
}
