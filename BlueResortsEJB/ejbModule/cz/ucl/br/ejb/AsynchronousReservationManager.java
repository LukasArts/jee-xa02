package cz.ucl.br.ejb;

import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.*;

@MessageDriven(name = "asynchronousReservationManager", activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(
                propertyName = "destination", propertyValue = "java:/jms/topic/BRReservationTopic")})

public class AsynchronousReservationManager implements MessageListener {
    private static final Logger log = Logger.getLogger(AsynchronousReservationManager.class.getName());

    @Resource(mappedName = "java:jboss/DefaultJMSConnectionFactory")
    private ConnectionFactory cf;

    @EJB
    private ReservationManager reservationManager;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void onMessage(Message incoming) {
        BytesMessage message = (BytesMessage) incoming;
        try (JMSContext context = cf.createContext()) {
            String accommodationId = incoming.getJMSCorrelationID();
            Long contractId = incoming.getLongProperty("contractId");

            String hotelName = incoming.getStringProperty("hotelName");
            String customerName = incoming.getStringProperty("customerName");
            int beds = message.readInt();
            Date dayFrom = new Date(message.readLong());
            Date dayTo = new Date(message.readLong());

            TextMessage msg = context.createTextMessage();
            msg.setJMSCorrelationID(accommodationId);
            msg.setLongProperty("contractId", contractId);

            try {
                String reservationCode = reservationManager.createReservation(hotelName, customerName, beds, dayFrom, dayTo);
                msg.setBooleanProperty("accepted", true);
                msg.setText(reservationCode);
            } catch (NoRoomAvailableException e) {
                msg.setBooleanProperty("accepted", false);
            }

            context.createProducer().send(incoming.getJMSReplyTo(),msg);
        } catch (JMSException e) {
            log.severe("Error while processing reservation " + e.getMessage());
        }

    }

}
