package cz.ucl.br.ejb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import cz.ucl.br.model.TaxiRequest;
import cz.ucl.br.model.TaxiResponse;

@MessageDriven(name = "taxiResponseCollector", activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(
                propertyName = "destination", propertyValue = "java:/jms/queue/BRTaxiResponseQueue")})
public class TaxiResponseCollector implements MessageListener {
    private static Logger log = Logger.getLogger(TaxiResponseCollector.class
            .getName());

    @PersistenceContext
    private EntityManager em;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void onMessage(Message receivedMessage) {
        try {
            TextMessage msg = (TextMessage) receivedMessage;

            Long requestId = new Long(msg.getJMSCorrelationID());
            log.info("Response received for request" + requestId);
            TaxiRequest request = em.find(TaxiRequest.class,
                    requestId);

            TaxiResponse response = new TaxiResponse();
            response.setRequest(request);
            response.setPrice(msg.getDoubleProperty("EndPrice"));
            response.setTaxiName(msg.getStringProperty("TaxiName"));
            response.setTaxiDescription(msg.getStringProperty("TaxiDescription"));

            em.persist(response);
        } catch (JMSException e) {
            log.severe("Taxi request sending failed: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

}
