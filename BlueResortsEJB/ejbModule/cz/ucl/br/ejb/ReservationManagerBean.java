package cz.ucl.br.ejb;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import cz.ucl.br.model.Hotel;
import cz.ucl.br.model.Room;
import cz.ucl.br.model.Stay;

@Remote(ReservationManager.class)
@Stateless
@Interceptors({ApplicationLogInterceptor.class})
public class ReservationManagerBean implements ReservationManager {
    private static final long DAY_IN_MILLISECONDS = 24 * 60 * 60 * 1000;

    @PersistenceContext
    private EntityManager em;

    @Resource
    private TimerService timerService;

    @SuppressWarnings("unchecked")
    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Hotel> findHotels(String location) {
        List<Hotel> result = null;

        result = em.createNamedQuery("Hotel.findByLocation").setParameter(
                "location", location).getResultList();

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String createReservation(String hotelName, String customerName,
                                    int bedCount, Date from, Date to) throws NoRoomAvailableException {

        // Najdi pokoje
        String queryText = "SELECT r FROM Room r WHERE r.hotel.name = :hotelName AND r.beds >= :bedCount AND NOT EXISTS ("
                + "  SELECT s FROM Stay s WHERE s.room = r AND ("
                + "    (s.dayFrom > :dateFrom AND s.dayFrom < :dateTo) OR "
                + "    (s.dayTo > :dateFrom AND s.dayTo <:dateTo) OR "
                + "    (:dateFrom > s.dayFrom AND :dateFrom < s.dayTo) OR "
                + "    (:dateTo > s.dayFrom AND :dateTo < s.dayTo) "
                + "  ) "
                + ") ORDER BY r.beds ASC";
        List<Room> availableRooms = (List<Room>) em.createQuery(queryText)
                .setParameter("hotelName", hotelName).setParameter("bedCount",
                        bedCount).setParameter("dateFrom", from).setParameter(
                        "dateTo", to).getResultList();

        // Založ rezervaci

        if (availableRooms.size() == 0) {
            throw new NoRoomAvailableException();
        } else {
            Room room = availableRooms.get(0);
            Stay stay = new Stay();
            stay.setCustomerName(customerName);
            stay.setDayFrom(from);
            stay.setDayTo(to);
            stay.setRoom(room);
            stay.setReservationCode(Long.toString(Math.abs((new Random())
                    .nextLong())));
            em.persist(stay);

            Calendar cleaningTime = Calendar.getInstance();
            cleaningTime.setTime(from);
            cleaningTime.set(Calendar.HOUR_OF_DAY, 10);
            cleaningTime.set(Calendar.MINUTE, 0);
            cleaningTime.set(Calendar.SECOND, 0);

            timerService.createTimer(cleaningTime.getTime(),
                    DAY_IN_MILLISECONDS, stay);

            return stay.getReservationCode();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void cancelReservation(String reservationCode)
            throws NoCancellationException {
        // Ověř čas do začátku pobytu
        Stay stay = null;
        List<Stay> reservationsFound = (List<Stay>) em.createNamedQuery(
                "Stay.findByReservationCode").setParameter("reservationCode",
                reservationCode).getResultList();

        if (reservationsFound.size() != 1)
            throw new NoCancellationException("No such reservation code");
        else
            stay = reservationsFound.get(0);

        Date now = new Date();
        long daysToStart = (stay.getDayFrom().getTime() - now.getTime())
                / (DAY_IN_MILLISECONDS);
        if (daysToStart < 7) {
            throw new NoCancellationException("Too late, only " + daysToStart
                    + " before start of your stay (minimum 7 to cancel)");
        } else {
            for (Timer t : timerService.getTimers()) {
                Stay timerStay = (Stay) t.getInfo();
                if (timerStay.getId() == stay.getId()) {
                    t.cancel();
                    break;
                }
            }
            em.remove(stay);
        }
    }

    @Timeout
    public void cleanRoom(Timer timer) {
        Stay stay = (Stay) timer.getInfo();
        System.out.println("Cleaning room " + stay.getRoom()
                + " for reservation " + stay.getReservationCode());
        if ((new Date()).after(stay.getDayTo()))
            timer.cancel();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getAllLocations() {
        return em.createNamedQuery("Hotel.allLocations").getResultList();
    }

}
