-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: brdb
-- ------------------------------------------------------
-- Server version	5.6.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applicationlogentry`
--

use brdb;

DROP TABLE IF EXISTS `applicationlogentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationlogentry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) DEFAULT NULL,
  `object` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicationlogentry`
--

LOCK TABLES `applicationlogentry` WRITE;
/*!40000 ALTER TABLE `applicationlogentry` DISABLE KEYS */;
INSERT INTO `applicationlogentry` VALUES (100,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-03-30 22:03:56','unknown'),(101,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-03-30 22:03:56','unknown'),(102,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-03-30 22:03:56','unknown'),(103,'public java.util.List cz.ucl.br.ejb.HotelManagerBean.listHotels()','cz.ucl.br.ejb.HotelManagerBean','2016-04-02 23:46:10','unknown'),(104,'public java.util.List cz.ucl.br.ejb.HotelManagerBean.listHotels()','cz.ucl.br.ejb.HotelManagerBean','2016-04-02 23:46:12','unknown'),(105,'public cz.ucl.br.model.Hotel cz.ucl.br.ejb.HotelManagerBean.selectHotel(cz.ucl.br.model.Hotel)','cz.ucl.br.ejb.HotelManagerBean','2016-04-02 23:46:12','unknown'),(106,'public java.util.List cz.ucl.br.ejb.HotelManagerBean.listHotels()','cz.ucl.br.ejb.HotelManagerBean','2016-04-03 09:40:49','unknown'),(107,'public java.util.List cz.ucl.br.ejb.HotelManagerBean.listHotels()','cz.ucl.br.ejb.HotelManagerBean','2016-04-03 10:25:23','unknown'),(108,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:54:10','unknown'),(109,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:54:11','unknown'),(110,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:54:11','unknown'),(111,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:40','unknown'),(112,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:42','unknown'),(113,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:43','unknown'),(114,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:43','unknown'),(115,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:46','unknown'),(116,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:46','unknown'),(117,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:46','unknown'),(118,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:46','unknown'),(119,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:46','unknown'),(120,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:46','unknown'),(121,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.findHotels(java.lang.String)','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:46','unknown'),(122,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.findHotels(java.lang.String)','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:46','unknown'),(123,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.findHotels(java.lang.String)','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:55:46','unknown'),(124,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:57:50','unknown'),(125,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.findHotels(java.lang.String)','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 18:57:50','unknown'),(126,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 19:09:46','unknown'),(127,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.findHotels(java.lang.String)','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 19:09:46','unknown'),(128,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 19:09:46','unknown'),(129,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 19:09:46','unknown'),(130,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.findHotels(java.lang.String)','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 19:09:46','unknown'),(131,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.findHotels(java.lang.String)','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 19:09:46','unknown'),(132,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 19:10:47','unknown'),(133,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.findHotels(java.lang.String)','cz.ucl.br.ejb.ReservationManagerBean','2016-04-06 19:10:47','unknown'),(134,'public java.util.List cz.ucl.br.ejb.HotelManagerBean.listHotels()','cz.ucl.br.ejb.HotelManagerBean','2016-04-06 19:11:40','unknown'),(135,'public java.util.List cz.ucl.br.ejb.HotelManagerBean.listHotels()','cz.ucl.br.ejb.HotelManagerBean','2016-04-06 19:11:43','unknown'),(136,'public cz.ucl.br.model.Hotel cz.ucl.br.ejb.HotelManagerBean.selectHotel(cz.ucl.br.model.Hotel)','cz.ucl.br.ejb.HotelManagerBean','2016-04-06 19:11:43','unknown'),(137,'Add a room','Room no. 302 on floor 3 with 1 bed(s)','2016-04-09 20:10:34','unknown'),(139,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-13 19:12:20','unknown'),(140,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-13 19:12:20','unknown'),(141,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-13 19:12:20','unknown'),(142,'public java.util.List cz.ucl.br.ejb.ReservationManagerBean.getAllLocations()','cz.ucl.br.ejb.ReservationManagerBean','2016-04-13 19:38:04','unknown');
/*!40000 ALTER TABLE `applicationlogentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (143),(143);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel`
--

LOCK TABLES `hotel` WRITE;
/*!40000 ALTER TABLE `hotel` DISABLE KEYS */;
INSERT INTO `hotel` VALUES (1,'The best hotel in town','Prague','Zlatá husa'),(2,'Flea hole','Prague','Hostel OneStar'),(3,'For a true gentleman','London','Ambassador'),(4,'A new discount hotel','Prague','New Prague'),(5,'A romantic hotel on the outskirts of the city','London','Nuovo Londono');
/*!40000 ALTER TABLE `hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `beds` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `floor` int(11) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `hotel_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK26F4FB67FB349E` (`hotel_id`),
  CONSTRAINT `FK26F4FB67FB349E` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (5,2,'First room',1,'101',1),(6,8,'Second room',1,'102',1),(8,5,'Fourth room',2,'201b',1),(9,4,'<p>Kr&aacute;sn&yacute; pokoj v druh&eacute;m patře s <span style=\"text-decoration: underline;\"><strong>v&yacute;hledem na Pražsk&yacute; hrad</strong></span></p>',2,'201',2);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `hotel_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKD97C5E9567FB349E` (`hotel_id`),
  CONSTRAINT `FKD97C5E9567FB349E` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stay`
--

DROP TABLE IF EXISTS `stay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customerName` varchar(255) DEFAULT NULL,
  `dayFrom` date DEFAULT NULL,
  `dayTo` date DEFAULT NULL,
  `reservationCode` varchar(255) DEFAULT NULL,
  `room_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK277A79C9C7F3F6` (`room_id`),
  CONSTRAINT `FK277A79C9C7F3F6` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stay`
--

LOCK TABLES `stay` WRITE;
/*!40000 ALTER TABLE `stay` DISABLE KEYS */;
INSERT INTO `stay` VALUES (2,'Martin Jelen','2009-05-04','2009-05-09','7544798748933372920',6),(5,'Michal Černý','2009-03-27','2009-03-31','6728824016250967360',5),(7,'Michal Černý','2009-03-27','2009-03-28','5666552133392180607',8),(8,'Ondřej Kučera','2009-03-15','2009-03-20','717943769022760960',5);
/*!40000 ALTER TABLE `stay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxirequest`
--

DROP TABLE IF EXISTS `taxirequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxirequest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `clientContact` varchar(255) DEFAULT NULL,
  `clientName` varchar(255) DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `fromPlace` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `toPlace` varchar(255) DEFAULT NULL,
  `selectedResponse_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2AD57E7138AF20F9` (`selectedResponse_id`),
  CONSTRAINT `FK2AD57E7138AF20F9` FOREIGN KEY (`selectedResponse_id`) REFERENCES `taxiresponse` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxirequest`
--

LOCK TABLES `taxirequest` WRITE;
/*!40000 ALTER TABLE `taxirequest` DISABLE KEYS */;
INSERT INTO `taxirequest` VALUES (1,'br@jelen.name','Michal Skrblík',8,'Hotel Zlatá Husa','Prague','Karolinská 1',NULL),(2,'br2@jelen.name','Michal Skrblík',11,'Hotel Zlatá Husa','Prague','Karolinská 1',NULL),(3,'vig951@unicorn.eu','aaa',1,'abc','Prague','def',NULL),(4,'vig951@unicorn.eu','kusta',15,'Hotel Zlatá Husa','Prague','Karolinská 1',NULL),(5,'vig951@unicorn.cz','Martin Jelen',14,'Hotel Zlatá Husa','Prague','Karolinská 1',NULL),(6,'temp@jelen.name','Zakaznik',12,'Odkud','Prague','Kam',NULL),(7,'mujemail@jelen.name','Ja',14,'Muj hotel','Prague','Nekam jinam',1),(8,'skrblik@jelen.name','Michal Skrblík',8,'Hotel Zlatá Husa','Prague','Karolinská 1',NULL),(9,'harry@jelen.name','Harry Falcon',11,'Hotel London Best Value','London','Houses of Parliament',2);
/*!40000 ALTER TABLE `taxirequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxiresponse`
--

DROP TABLE IF EXISTS `taxiresponse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxiresponse` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `price` double DEFAULT NULL,
  `taxiDescription` varchar(255) DEFAULT NULL,
  `taxiName` varchar(255) DEFAULT NULL,
  `request_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3302049FCD758BC0` (`request_id`),
  CONSTRAINT `FK3302049FCD758BC0` FOREIGN KEY (`request_id`) REFERENCES `taxirequest` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxiresponse`
--

LOCK TABLES `taxiresponse` WRITE;
/*!40000 ALTER TABLE `taxiresponse` DISABLE KEYS */;
INSERT INTO `taxiresponse` VALUES (1,378,'Bila Skoda Fabia','Vlastimil Zrada',7),(2,220,'Žlutý Renault Clio','Michal Zelený',9),(3,297,'Bila Skoda Fabia','Vlastimil Zrada',9);
/*!40000 ALTER TABLE `taxiresponse` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-13 19:50:34
