package cz.ucl.bt.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import cz.ucl.bt.model.types.ContractStatus;

/**
 * Entity implementation class for Entity: Contract
 */
@Entity

public class Contract implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private long number;

    @Enumerated(EnumType.STRING)
    private ContractStatus status;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Customer customer;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Holiday holiday;

    @OneToMany(mappedBy = "contract")
    private Set<StayConfirmation> hotelConfirmations;

    @OneToMany(mappedBy = "contract", cascade = CascadeType.ALL)
    private Set<Traveller> travellers;

    @Enumerated(EnumType.ORDINAL)
    private ConfirmationStatus confirmed;

    private static final long serialVersionUID = 1L;

    public Contract() {
        super();
        travellers = new HashSet<Traveller>();
        hotelConfirmations = new HashSet<StayConfirmation>();
        confirmed = ConfirmationStatus.Undecided;
    }

    public Long getId() {
        return this.id;
    }

    public long getNumber() {
        return this.number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<Traveller> getTravellers() {
        return this.travellers;
    }

    public void addTraveller(Traveller t) {
        travellers.add(t);
        t.setContract(this);
    }

    public Holiday getHoliday() {
        return holiday;
    }

    public void setHoliday(Holiday holiday) {
        this.holiday = holiday;
    }

    public ContractStatus getStatus() {
        return status;
    }

    public void setStatus(ContractStatus status) {
        this.status = status;
    }

    public Set<StayConfirmation> getHotelConfirmations() {
        return hotelConfirmations;
    }

    public void setConfirmed(ConfirmationStatus b) {
        this.confirmed = b;
    }

    public ConfirmationStatus getConfirmed() {
        return confirmed;
    }


}
