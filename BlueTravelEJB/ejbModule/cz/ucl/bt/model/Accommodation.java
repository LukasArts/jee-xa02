package cz.ucl.bt.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Stay
 */
@Entity

public class Accommodation implements Serializable {


    @Id
    @GeneratedValue
    private Long id;

    private String location;
    private String hotelName;

    @Temporal(TemporalType.DATE)
    private Date dayFrom;

    @Temporal(TemporalType.DATE)
    private Date dayTo;

    @ManyToOne
    private Holiday holiday;

    private static final long serialVersionUID = 1L;

    public Accommodation() {
        super();
    }

    public Long getId() {
        return this.id;
    }

    public Date getDayFrom() {
        return dayFrom;
    }

    public void setDayFrom(Date dayFrom) {
        this.dayFrom = dayFrom;
    }

    public Date getDayTo() {
        return dayTo;
    }

    public void setDayTo(Date dayTo) {
        this.dayTo = dayTo;
    }

    public Holiday getHoliday() {
        return holiday;
    }

    public void setHoliday(Holiday holiday) {
        this.holiday = holiday;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHotelName() {
        return this.hotelName;
    }

    public void setHotelName(String name) {
        this.hotelName = name;
    }

    @Override
    public String toString() {
        String result = "";

        result += getHotelName();
        result += ", from ";
        DateFormat df = DateFormat.getDateInstance();
        result += getDayFrom() != null ? df.format(getDayFrom()) : "null";
        result += " to ";
        result += getDayTo() != null ? df.format(getDayTo()) : "null";
        return result;
    }

}
