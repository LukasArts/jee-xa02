package cz.ucl.bt.model;

public enum ConfirmationStatus {
    Undecided, Confirmed, Refused
}
