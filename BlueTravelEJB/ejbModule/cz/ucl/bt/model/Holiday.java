package cz.ucl.bt.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: Holiday
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Trip.getCount", query = "SELECT COUNT(h) FROM Holiday h WHERE fixed = true"),
        @NamedQuery(name = "Trip.getFixed", query = "SELECT h FROM Holiday h WHERE fixed = true"),
        @NamedQuery(name = "Trip.getAllNamesAndIds", query = "SELECT h.name, h.id FROM Holiday h WHERE fixed = true"),
        @NamedQuery(name = "Trip.findByName", query = "SELECT h FROM Holiday h WHERE fixed = true AND name = :name")
})
public class Holiday implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private Boolean fixed = true;

    private Boolean confirmed = false;

    @Temporal(TemporalType.DATE)
    private Date starts;

    @Temporal(TemporalType.DATE)
    private Date ends;

    private Double price;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Transportation> transportation;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Service> services;

    @OneToMany(mappedBy = "holiday", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderBy("dayFrom ASC")
    private Set<Accommodation> accommodation;

    private static final long serialVersionUID = 1L;

    public Holiday() {
        super();
        transportation = new HashSet<Transportation>();
        accommodation = new HashSet<Accommodation>();
        services = new HashSet<Service>();
    }

    public Long getId() {
        return this.id;
    }

    public boolean getFixed() {
        return this.fixed;
    }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    public Set<Transportation> getTransportation() {
        return this.transportation;
    }

    public Set<Accommodation> getAccommodation() {
        return this.accommodation;
    }

    public void addAccommodation(Accommodation stay) {
        accommodation.add(stay);
        stay.setHoliday(this);
    }

    public void addTransportation(Transportation transport) {
        transportation.add(transport);
    }

    public Set<Service> getServices() {
        return services;
    }

    public void addService(Service service) {
        services.add(service);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getEnds() {
        return ends;
    }

    public void setEnds(Date ends) {
        this.ends = ends;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getStarts() {
        return starts;
    }

    public void setStarts(Date starts) {
        this.starts = starts;
    }

    public void setAccommodation(Set<Accommodation> accommodation) {
        this.accommodation = accommodation;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }


}
