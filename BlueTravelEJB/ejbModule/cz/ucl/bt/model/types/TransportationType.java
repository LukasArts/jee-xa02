package cz.ucl.bt.model.types;

public enum TransportationType {
    Airplane, Bus, Train, Combined
}
