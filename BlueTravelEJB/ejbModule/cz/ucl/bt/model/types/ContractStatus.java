package cz.ucl.bt.model.types;

public enum ContractStatus {
    Pending, Confirmed, AccommodationRefused
}
