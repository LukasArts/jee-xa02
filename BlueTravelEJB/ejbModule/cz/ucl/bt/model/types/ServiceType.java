package cz.ucl.bt.model.types;

public enum ServiceType {
    Trip, Spa, Other
}
