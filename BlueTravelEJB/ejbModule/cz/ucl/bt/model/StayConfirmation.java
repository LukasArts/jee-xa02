package cz.ucl.bt.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Entity implementation class for Entity: Stay
 */
@Entity
public class StayConfirmation implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Accommodation stay;

    @ManyToOne
    private Contract contract;

    private String reason;
    private boolean accepted;

    private static final long serialVersionUID = 1L;

    public StayConfirmation() {
        super();
    }

    public Long getId() {
        return this.id;
    }

    public String getReason() {
        return reason;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public StayConfirmation(Contract contract, Accommodation stay, boolean accepted, String reason) {
        super();

        this.accepted = accepted;
        this.reason = reason;
        this.stay = stay;
        this.contract = contract;
    }


}
