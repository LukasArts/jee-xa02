package cz.ucl.bt.ejb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cz.ucl.br.ejb.ReservationManager;
import cz.ucl.br.model.Hotel;
import cz.ucl.bt.model.Holiday;
import cz.ucl.bt.model.Accommodation;
import cz.ucl.bt.model.Transportation;

@Stateless
public class TripConfigurationManagerBean implements TripConfigurationManager {

    @PersistenceContext
    private EntityManager em;

    @EJB(mappedName = "java:global/BlueResortsEAR_ear/BlueResortsEJB_ejb/ReservationManagerBean!cz.ucl.br.ejb.ReservationManager")
    private ReservationManager reservationManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Holiday> getFixedTrips() {
        return (List<Holiday>) em.createNamedQuery("Trip.getFixed")
                .getResultList();
    }

    @Override
    public Holiday loadCompleteTrip(Long currentHolidayId) {
        Holiday currentHoliday = em.find(Holiday.class, currentHolidayId);
        currentHoliday.getAccommodation().size();
        currentHoliday.getTransportation().size();
        currentHoliday.getServices().size();
        return currentHoliday;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getAllFromCities() {
        List<String> result = (List<String>) em.createNamedQuery(
                "Transportation.findAllFromLocations").getResultList();
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getAllToCities() {
        List<String> result = (List<String>) em.createNamedQuery(
                "Transportation.findAllToLocations").getResultList();
        return result;
    }

    @Override
    public Holiday saveHoliday(Holiday currentHoliday) {
        Holiday result = null;

        if (currentHoliday.getId() != null)
            result = em.merge(currentHoliday);
        else {
            em.persist(currentHoliday);
            result = currentHoliday;
        }

        return result;
    }

    @Override
    public Transportation loadTransportation(Long transportId) {
        return em.find(Transportation.class, transportId);
    }

    @Override
    public Transportation removeTransportFromTrip(Long tripId, Long transportId) {
        Holiday trip = em.find(Holiday.class, tripId);
        Transportation transport = em.find(Transportation.class, transportId);
        trip.getTransportation().remove(transport);

        return transport;
    }

    @Override
    public Accommodation removeAccommodationFromTrip(Long tripId, Long accommodationId) {
        Holiday trip = em.find(Holiday.class, tripId);
        Accommodation accommodation = em.find(Accommodation.class, accommodationId);
        trip.getAccommodation().remove(accommodation);
        em.remove(em.find(Accommodation.class, accommodationId));

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Transportation> findTransportsByExample(
            Transportation transportExample) {
        String qString = "SELECT t FROM Transportation t WHERE t.from = :transFrom AND t.to = :transTo ";
        if (transportExample.getDeparture() != null)
            qString += " AND t.departure > :dep_from AND t.departure < :dep_to ";


        Query q = em.createQuery(qString);
        q.setParameter("transFrom", transportExample.getFrom());
        q.setParameter("transTo", transportExample.getTo());
        if (transportExample.getDeparture() != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(transportExample.getDeparture());

            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);

            Date day1 = new Date(cal.getTimeInMillis());
            q.setParameter("dep_from", day1);

            cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1);
            Date day2 = new Date(cal.getTimeInMillis());
            q.setParameter("dep_to", day2);
        }

        List<Transportation> result = (List<Transportation>) q.getResultList();
        return result;
    }

    @Override
    public List<String> getAllHotelCities() {
        return reservationManager.getAllLocations();
    }

    @Override
    public List<String> getHotelsForCity(String selectedCity) {
        List<String> result = new ArrayList<String>();
        List<Hotel> allHotels = reservationManager.findHotels(selectedCity);
        for (Hotel h : allHotels) result.add(h.getName());
        return result;
    }

    @Override
    public void removeHoliday(Long currentHolidayId) {
        em.remove(em.find(Holiday.class, currentHolidayId));
    }

    @Override
    public Accommodation addAccommodation(Long currentHolidayId,
                                          Accommodation currentAccomodation) {
        Holiday trip = em.find(Holiday.class,currentHolidayId);
        trip.addAccommodation(currentAccomodation);

        return null;
    }

}
