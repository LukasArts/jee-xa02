package cz.ucl.bt.ejb;

import java.util.List;

import javax.ejb.Local;

import cz.ucl.bt.model.Accommodation;
import cz.ucl.bt.model.Holiday;
import cz.ucl.bt.model.Transportation;

@Local
public interface TripConfigurationManager {

    List<Holiday> getFixedTrips();

    Holiday loadCompleteTrip(Long currentHolidayId);

    List<String> getAllFromCities();

    List<String> getAllToCities();

    Holiday saveHoliday(Holiday currentHoliday);

    Transportation loadTransportation(Long transportId);

    Transportation removeTransportFromTrip(Long id, Long transportId);

    List<Transportation> findTransportsByExample(Transportation transportExample);

    List<String> getAllHotelCities();

    List<String> getHotelsForCity(String selectedCity);

    Accommodation removeAccommodationFromTrip(Long id, Long accommodationId);

    void removeHoliday(Long currentHolidayId);

    Accommodation addAccommodation(Long currentHolidayId,
                                   Accommodation currentAccomodation);

}
