package cz.ucl.bt.ejb;

import cz.ucl.bt.model.Accommodation;
import cz.ucl.bt.model.ConfirmationStatus;
import cz.ucl.bt.model.Contract;
import cz.ucl.bt.model.StayConfirmation;

import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(name = "hotelReservationResponseReceiver", activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(
                propertyName = "destination", propertyValue = "java:/jms/queue/BTReservationResponseQueue")})
public class HotelReservationResponseReceiver implements MessageListener {
    private static final Logger log = Logger
            .getLogger(HotelReservationResponseReceiver.class.getName());

    @EJB
    private ContractManager contractManager;


    @Override
    public void onMessage(Message response) {
        try {
            String accommodationId = response.getJMSCorrelationID();
            String reservationCode = ((TextMessage) response).getText();
            Contract contract = contractManager.loadContract(response.getLongProperty("contractId"));
            Boolean accepted = response.getBooleanProperty("accepted");

            Set<Accommodation> accommodations = contract.getHoliday().getAccommodation();
            Set<StayConfirmation> confirmations = contract.getHotelConfirmations();

            for (Accommodation a : accommodations)
                if (a.getId().equals(Long.parseLong(accommodationId))) {
                    confirmations.add(new StayConfirmation(contract, a, accepted, reservationCode));
                    log.info("Accommodation " + a.getId() + " (" + a + ") for contract " + contract.getId() + " confirmed.");
                }

            // pokud máme všechna ubytování potvrzena nebo zamítnuta
            Boolean contractConfirmed = true;
            if (accommodations.size() == confirmations.size()) {
                for (StayConfirmation c : confirmations)
                    if (!c.isAccepted()) contractConfirmed = false;

                if (contractConfirmed) {
                    contract.setConfirmed(ConfirmationStatus.Confirmed);
                    log.info("Contract " + contract.getId() + " (" + contract.getHoliday().getName() + ") confirmed.");
                } else {
                    contract.setConfirmed(ConfirmationStatus.Refused);
                    log.info("Contract " + contract.getId() + " (" + contract.getHoliday().getName() + ") refused.");
                }
            }
        } catch (JMSException ex) {
            throw new RuntimeException(ex);
        }
    }

}
