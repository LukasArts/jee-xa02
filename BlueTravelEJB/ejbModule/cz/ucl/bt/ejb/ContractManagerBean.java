package cz.ucl.bt.ejb;

import cz.ucl.bt.model.Contract;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ContractManagerBean implements ContractManager {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Contract loadContract(Long contractId) {
        return em.find(Contract.class,contractId);
    }
}
