package cz.ucl.bt.ejb;

import javax.ejb.Local;
import cz.ucl.bt.model.Contract;

@Local
public interface ContractManager {

    Contract loadContract(Long contractId);
}
