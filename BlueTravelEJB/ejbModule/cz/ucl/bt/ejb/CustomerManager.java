package cz.ucl.bt.ejb;

import java.util.List;

import javax.ejb.Local;

import cz.ucl.bt.model.Customer;

@Local
public interface CustomerManager {
    List<Customer> findCustomersBySurnamePrefix(String surnamePrefix);

    Customer loadCustomer(Long customerId);

}
