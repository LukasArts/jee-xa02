package cz.ucl.bt.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import cz.ucl.bt.model.Contract;
import cz.ucl.bt.model.Customer;
import cz.ucl.bt.model.StayConfirmation;

@Stateless
public class CustomerManagerBean implements CustomerManager {
    @PersistenceContext
    private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public List<Customer> findCustomersBySurnamePrefix(String surnamePrefix) {
        List<Customer> result = null;
        result = (List<Customer>) em.createNamedQuery("Customer.findBySurname")
                .setParameter("surname", surnamePrefix + "%").getResultList();
        return result;
    }

    @Override
    public Customer loadCustomer(Long customerId) {
        Customer result = (Customer) em.createQuery(
                "SELECT c FROM Customer c "
                        + "LEFT JOIN FETCH c.contracts contract "
                        + "LEFT JOIN FETCH contract.holiday holiday "
                        + "WHERE c.id = :id").setParameter("id", customerId)
                .getSingleResult();
        return result;
    }

}
