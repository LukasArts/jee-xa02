package cz.ucl.bt.ejb;

import java.util.Map;

import javax.ejb.Local;

import cz.ucl.bt.model.Contract;
import cz.ucl.bt.model.Customer;
import cz.ucl.bt.model.Holiday;

@Local
public interface PurchaseManager {

    Holiday loadHoliday(long selectedTripId);

    Map<String, Long> getTripNameIdMap();

    Customer loadCustomer(long customerId);

    void saveContractExistingCustomer(Contract contract,
                                      Long existingCustomerId, long holidayId);

    void saveContractNewCustomer(Contract contract, long holidayId);
}
