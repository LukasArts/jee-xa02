package cz.ucl.bt.ejb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import cz.ucl.bt.model.Contract;
import cz.ucl.bt.model.Customer;
import cz.ucl.bt.model.Holiday;
import cz.ucl.bt.model.Accommodation;

@Stateless
public class PurchaseManagerBean implements PurchaseManager {
    private static final Logger log = Logger.getLogger(PurchaseManagerBean.class.getName());
    @PersistenceContext
    private EntityManager em;

    @Resource(mappedName = "java:/jms/topic/BRReservationTopic")
    private Destination requestTopic;

    @Resource(mappedName = "java:/jms/queue/BTReservationResponseQueue")
    private Destination responseQueue;

    @Resource(mappedName = "java:jboss/DefaultJMSConnectionFactory")
    private ConnectionFactory cf;

    @Override
    public Holiday loadHoliday(long selectedTripId) {
        Holiday selectedHoliday = em.find(Holiday.class, selectedTripId);
        selectedHoliday.getAccommodation().size();
        selectedHoliday.getTransportation().size();
        selectedHoliday.getServices().size();
        return selectedHoliday;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Long> getTripNameIdMap() {
        Map<String, Long> allTrips = new HashMap<>();
        List<Object[]> queryResult = em.createNamedQuery(
                "Trip.getAllNamesAndIds").getResultList();
        for (Object[] item : queryResult) {
            allTrips.put((String) item[0], (Long) item[1]);
        }
        return allTrips;
    }

    @Override
    public Customer loadCustomer(long customerId) {
        Customer selectedCustomer = em.find(Customer.class, customerId);
        return selectedCustomer;
    }

    @Override
    public void saveContractExistingCustomer(Contract contract,
                                             Long existingCustomerId, long holidayId) {
        contract.setCustomer(em.find(Customer.class, existingCustomerId));
        contract.setHoliday(em.find(Holiday.class, holidayId));
        saveContract(contract);
    }

    @Override
    public void saveContractNewCustomer(Contract contract, long holidayId) {
        contract.setHoliday(em.find(Holiday.class, holidayId));
        saveContract(contract);
    }

    private void saveContract(Contract contract) {
        em.persist(contract);
        for (Accommodation s : contract.getHoliday().getAccommodation()) {
            try {
                Connection con = cf.createConnection();
                Session sess = con.createSession(false, Session.AUTO_ACKNOWLEDGE);

                MessageProducer sender = sess.createProducer(requestTopic);

                BytesMessage msg = sess.createBytesMessage();
                msg.setLongProperty("contractId", contract.getId());
                msg.setStringProperty("customerName", contract.getCustomer().getName());
                msg.setStringProperty("hotelName", s.getHotelName());
                msg.setJMSCorrelationID(s.getId().toString());
                msg.setJMSReplyTo(responseQueue);
                //beds
                msg.writeInt(contract.getTravellers().size());
                //from
                msg.writeLong(s.getDayFrom() != null ? s.getDayFrom().getTime() : null);
                //to
                msg.writeLong(s.getDayTo() != null ? s.getDayTo().getTime() : null);
                sender.send(msg);

                sender.close();
                sess.close();
                con.close();

                log.info("Confirmation request " + s.getId() + " (contract " + contract.getId() + ")sent.");
            } catch (JMSException e) {
                log.severe("Confirmation request sending failed: " + e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }

}
