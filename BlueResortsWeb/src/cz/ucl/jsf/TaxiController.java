package cz.ucl.jsf;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import cz.ucl.br.ejb.TaxiRequestManager;
import cz.ucl.br.model.TaxiRequest;

@ManagedBean(name = "taxiController")
@ViewScoped
public class TaxiController implements Serializable {
    @EJB
    private TaxiRequestManager manager;

    private TaxiRequest request;

    public TaxiController() {
        super();

        request = new TaxiRequest();
    }

    public TaxiRequest getRequest() {
        return request;
    }

    public String sendRequest() {
        Long requestId = manager.requestTaxi(request);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Taxi request sent (ID " + requestId + ")", "Taxi request sent (ID " + requestId + ")"));
        return "request_sent";
    }

}
