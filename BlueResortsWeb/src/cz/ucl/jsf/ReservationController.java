package cz.ucl.jsf;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import cz.ucl.br.ejb.NoCancellationException;
import cz.ucl.br.ejb.NoRoomAvailableException;
import cz.ucl.br.ejb.ReservationManager;
import cz.ucl.br.model.Hotel;

@ManagedBean(name = "reservationController")
@ViewScoped
public class ReservationController implements Serializable {
    @EJB
    private ReservationManager manager;

    private String location;

    private String hotelName;
    private String customerName;
    private int bedCount = 2;
    private Date from;
    private Date to;

    private String reservationCode;

    private List<String> allLocations;
    private List<Hotel> localHotels;

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getBedCount() {
        return bedCount;
    }

    public void setBedCount(int bedCount) {
        this.bedCount = bedCount;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getReservationCode() {
        return reservationCode;
    }

    public void setReservationCode(String reservationCode) {
        this.reservationCode = reservationCode;
    }

    public List<String> getAllLocations() {
        if (allLocations == null) {
            allLocations = manager.getAllLocations();
            setLocation(allLocations.get(0));
        }
        return allLocations;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        if ((location != null) && (!location.equals(this.location))) {
            localHotels = manager.findHotels(location);
        }
        this.location = location;
    }

    public List<Hotel> getLocalHotels() {
        return localHotels;
    }

    public String createReservation() {
        try {
            String newReservationCode = manager.createReservation(hotelName, customerName, bedCount, from, to);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Reservation created with code " + newReservationCode, "Reservation created"));
            return "reservation_created";
        } catch (NoRoomAvailableException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No rooms available: ", "No rooms available for your request - please try another date"));
            return null;
        }
    }

    public String cancelReservation() {
        try {
            manager.cancelReservation(reservationCode);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Reservation cancelled", "Reservation with code " + reservationCode + " was cancelled"));
            return "reservation_cancelled";
        } catch (NoCancellationException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cancellation not possible: ", e.getReason()));
            return null;
        }
    }

}
