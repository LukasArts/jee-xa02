package cz.ucl.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import cz.ucl.br.ejb.HotelManager;
import cz.ucl.br.model.Hotel;
import cz.ucl.br.model.Room;

@ManagedBean(name = "hotelController")
@ViewScoped
public class HotelController implements Serializable {
    @EJB
    private HotelManager manager;

    private Map<Long, Hotel> allHotels;
    private Long selectedHotelId;
    private Hotel selectedHotel;

    private Long selectedRoomId;
    private Room room;

    private void initHotels() {
        if (allHotels == null) {
            allHotels = new TreeMap<Long, Hotel>();
            for (Hotel h : manager.listHotels())
                allHotels.put(h.getId(), h);
        }
    }

    public List<Hotel> getHotelList() {
        initHotels();

        return new ArrayList<Hotel>(allHotels.values());
    }

    public void setSelectedHotelId(Long selectedHotelId) {
        if (selectedHotelId != null && !selectedHotelId.equals(this.selectedHotelId)) {
            this.selectedHotelId = selectedHotelId;
            initHotels();
            selectedHotel = manager.selectHotel(allHotels.get(selectedHotelId));
        } else if (selectedHotelId == null && this.selectedHotelId != null) {
            this.selectedHotelId = null;
            this.selectedHotel = null;
        }
    }

    public Long getSelectedHotelId() {
        return this.selectedHotelId;
    }

    public boolean isHotelSelected() {
        return (selectedHotel != null);
    }

    public Hotel getSelectedHotel() {
        return selectedHotel;
    }

    public String closeHotel() {
        manager.closeHotel();

        selectedHotel = null;
        selectedHotelId = null;

        return "hotel_closed";
    }

    public void setSelectedRoomId(Long selectedRoomId) {
        if (selectedRoomId != null
                && (!selectedRoomId.equals(this.selectedRoomId))) {
            for (Room r : selectedHotel.getRooms()) {
                if (r.getId().equals(selectedRoomId)) {
                    room = r;
                    break;
                }
            }
            this.selectedRoomId = selectedRoomId;
        } else if (selectedRoomId == null && this.selectedRoomId != null) {
            this.selectedRoomId = null;
            this.room = null;
        }
    }

    public Long getSelectedRoomId() {
        return this.selectedRoomId;
    }

    public boolean isRoomSelected() {
        return (selectedRoomId != null);
    }

    public Room getRoom() {
        if (room == null)
            room = new Room();
        return room;
    }

    public String addRoom() {
        manager.addRoom(room);
        selectedHotel.getRooms().add(room);
        room = null;
        return "room_added";
    }

    public String changeRoom() {
        manager.changeRoom(room);
        return "room_changed";
    }

    public String removeRoom() {
        manager.removeRoom(room);
        selectedHotel.getRooms().remove(room);
        return "room_removed";
    }

}
