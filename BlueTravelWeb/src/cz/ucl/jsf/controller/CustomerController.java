package cz.ucl.jsf.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import cz.ucl.bt.ejb.CustomerManager;
import cz.ucl.bt.model.ConfirmationStatus;
import cz.ucl.bt.model.Customer;

@ManagedBean(name = "customerController")
@ViewScoped
public class CustomerController implements Serializable {
    private static final long serialVersionUID = -4646329764605891472L;

    private String searchSurname;
    private List<Customer> customersFound;
    private Long selectedCustomerId;
    private Customer currentCustomer;

    @EJB
    private CustomerManager manager;

    public String getSearchSurname() {
        return searchSurname;
    }

    public void setSearchSurname(String searchSurname) {
        this.searchSurname = searchSurname;
    }

    public Long getSelectedCustomerId() {
        return selectedCustomerId;
    }

    public void setSelectedCustomerId(Long selectedClientId) {
        this.selectedCustomerId = selectedClientId;
    }

    public List<Customer> getCustomersFound() {
        return customersFound;
    }

    public Customer getCurrentCustomer() {
        if (currentCustomer == null) {
            currentCustomer = manager.loadCustomer(selectedCustomerId);
        }

        return currentCustomer;
    }

    public void searchCustomers() {
        customersFound = manager.findCustomersBySurnamePrefix(searchSurname);
    }

    public Map<ConfirmationStatus, String> getBackgroundColor() {
        Map<ConfirmationStatus, String> colors = new HashMap<ConfirmationStatus, String>();
        colors.put(ConfirmationStatus.Undecided, "yellow");
        colors.put(ConfirmationStatus.Confirmed, "#7FFFD4");
        colors.put(ConfirmationStatus.Refused, "#FA8072");
        return colors;
    }

    @PostConstruct
    public void init() {
        System.out.println("!!! Customer Controller initialized for VIEW SCOPE: " + this.toString());
    }

    @PreDestroy
    public void destroy() {
        System.out.println("!!! Customer Controller destroyed at the end of the VIEW SCOPE: " + this.toString());
    }
}
