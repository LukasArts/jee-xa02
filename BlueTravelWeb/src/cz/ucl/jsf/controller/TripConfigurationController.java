package cz.ucl.jsf.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import cz.ucl.bt.ejb.TripConfigurationManager;
import cz.ucl.bt.model.Accommodation;
import cz.ucl.bt.model.Holiday;
import cz.ucl.bt.model.Transportation;

@ManagedBean(name = "holidayManager")
@SessionScoped
public class TripConfigurationController implements Serializable {
    private static final long serialVersionUID = 8691026176654134193L;

    private Holiday currentHoliday;
    private Accommodation currentAccommodation;
    private Transportation transportExample;

    private Long currentHolidayId;
    private Long transportId;
    private Long accommodationId;

    @EJB
    private TripConfigurationManager manager;


    public List<Holiday> getAllHolidays() {
        return manager.getFixedTrips();
    }

    public Holiday getCurrentHoliday() {
        if (currentHoliday == null) {
            if (currentHolidayId != null) {
                currentHoliday = manager.loadCompleteTrip(currentHolidayId);
            } else {
                currentHoliday = new Holiday();
            }
        }

        return currentHoliday;
    }


    public Accommodation getCurrentAccommodation() {
        if (currentAccommodation == null) {
            currentAccommodation = new Accommodation();
        }
        return currentAccommodation;
    }

    public void setCurrentHolidayId(Long id) {
        if (id != null && !id.equals(currentHolidayId)) {
            currentHolidayId = id;

            transportId = null;
            accommodationId = null;

            currentHoliday = null;
            currentAccommodation = null;
            transportExample = null;

            currentHoliday = getCurrentHoliday();
            currentAccommodation = getCurrentAccommodation();
            transportExample = getTransportExample();
        }

    }

    public Long getCurrentHolidayId() {
        return currentHolidayId;
    }

    public Transportation getTransportExample() {
        if (transportExample == null)
            transportExample = new Transportation();
        return transportExample;
    }

    public void setAccommodationId(Long accommodationId) {
        this.accommodationId = accommodationId;
    }

    public void setTransportId(Long transportId) {
        this.transportId = transportId;
    }

    public List<String> getAllFromCities() {
        return manager.getAllFromCities();
    }

    public List<String> getAllToCities() {
        return manager.getAllToCities();
    }

    public List<String> getAllHotelCities() {
        return manager.getAllHotelCities();
    }

    public List<String> getHotelsForSelectedCity() {
        if (getCurrentAccommodation().getLocation() != null && !"".equals(getCurrentAccommodation().getLocation()))
            return manager.getHotelsForCity(getCurrentAccommodation().getLocation());
        else return new ArrayList<>();
    }

    public void removeTransport() {
        manager.removeTransportFromTrip(currentHoliday.getId(), transportId);

        for (Transportation t : getCurrentHoliday().getTransportation()) {
            if (t.getId().equals(transportId)) {
                getCurrentHoliday().getTransportation().remove(t);
                break;
            }
        }

    }

    public String saveHoliday() {
        currentHoliday = manager.saveHoliday(currentHoliday);

        return "holiday_saved";
    }

    public void addTransport() {
        currentHoliday.getTransportation().add(manager.loadTransportation(this.transportId));
        currentHoliday = manager.saveHoliday(currentHoliday);
    }

    public void addAccommodation() {
        if (currentAccommodation.getLocation() != null) {
            manager.addAccommodation(getCurrentHolidayId(),currentAccommodation);
            currentHoliday.addAccommodation(currentAccommodation);
            currentAccommodation = null;
        }
    }

    public void removeAccommodation() {
        manager.removeAccommodationFromTrip(currentHoliday.getId(),accommodationId);

        for (Accommodation a : getCurrentHoliday().getAccommodation()) {
            if (a.getId().equals(accommodationId)) {
                getCurrentHoliday().getAccommodation().remove(a);
                break;
            }
        }

    }

    public List<Transportation> getFoundTransports() {
        return manager.findTransportsByExample(getTransportExample());
    }

    public String removeHoliday() {
        manager.removeHoliday(currentHolidayId);
        return null;
    }

}
