package cz.ucl.jsf.controller;

import java.io.Serializable;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import cz.ucl.bt.ejb.PurchaseManager;
import cz.ucl.bt.model.Contract;
import cz.ucl.bt.model.CreditCard;
import cz.ucl.bt.model.Customer;
import cz.ucl.bt.model.Traveller;

@ManagedBean(name = "purchaseController")
@SessionScoped
public class PurchaseController implements Serializable {
    private static final long serialVersionUID = -8118331554543394402L;
    @EJB
    private PurchaseManager manager;

    private Contract contract;
    private Traveller currentTraveller;
    private long selectedTravellerId;

    private Map<String, Long> allTrips;

    private String[] creditCardNo = new String[4];
    private String[] creditCardValidity = new String[2];

    public Contract getContract() {
        if (contract == null) {
            contract = new Contract();

            Customer customer = new Customer();
            contract.setCustomer(customer);
        }
        return contract;
    }

    public Traveller getCurrentTraveller() {
        if (currentTraveller == null) {
            currentTraveller = new Traveller();
        }
        return currentTraveller;
    }

    public void setSelectedTravellerId(Long selectedTravellerId) {
        this.selectedTravellerId = selectedTravellerId;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Map<String, Long> getAllTrips() {
        if (allTrips == null) {
            allTrips = manager.getTripNameIdMap();
            allTrips.put("", -1L);
        }
        return allTrips;
    }

    public String[] getCreditCardNo() {
        return creditCardNo;
    }

    public void setCreditCardNo(String[] creditCardNo) {
        this.creditCardNo = creditCardNo;
    }

    public String[] getCreditCardValidity() {
        return creditCardValidity;
    }

    public void setCreditCardValidity(String[] creditCardValidity) {
        this.creditCardValidity = creditCardValidity;
    }

    public long getSelectedTripId() {
        if (getContract().getHoliday() != null)
            return getContract().getHoliday().getId();
        else
            return -1;
    }

    public void setSelectedTripId(long selectedTripId) {
        if (selectedTripId != -1) {
            contract.setHoliday(manager.loadHoliday(selectedTripId));
        }
    }

    public void selectTraveller() {
        for (Traveller traveller : contract.getTravellers()) {
            if (traveller.getId() != null
                    && traveller.getId() == selectedTravellerId) {
                currentTraveller = traveller;
                break;
            } else if (traveller.getId() == null
                    && traveller.hashCode() == selectedTravellerId) {
                currentTraveller = traveller;
                break;
            }

        }
    }

    public void saveTraveller() {
        contract.addTraveller(currentTraveller);
        currentTraveller = new Traveller();
    }

    public void newTraveller() {
        currentTraveller = new Traveller();
    }

    public int travellerHashCode(Traveller t) {
        return t.hashCode();
    }

    public void removeTraveller() {
        Traveller selectedTraveller = null;
        for (Traveller traveller : contract.getTravellers()) {
            if (traveller.getId() != null
                    && traveller.getId() == selectedTravellerId) {
                selectedTraveller = traveller;
                break;
            } else if (traveller.getId() == null
                    && traveller.hashCode() == selectedTravellerId) {
                selectedTraveller = traveller;
                break;
            }
        }

        contract.getTravellers().remove(selectedTraveller);

        if (currentTraveller == selectedTraveller)
            currentTraveller = new Traveller();
    }

    public void copyTravellerFromCustomer() {
        currentTraveller = new Traveller();
        currentTraveller.setSurname(contract.getCustomer().getSurname());
        currentTraveller.setFirstName(contract.getCustomer().getFirstName());

    }

    public Double getCalculatedPrice() {
        if (contract.getHoliday() != null) {
            Double basePrice = contract.getHoliday().getPrice();
            return basePrice * contract.getTravellers().size();
        } else {
            return null;
        }
    }

    public String confirmContract() {

        CreditCard card = new CreditCard();
        card.setNumber(creditCardNo[0] + creditCardNo[1] + creditCardNo[2]
                + creditCardNo[3]);

        if (card.getNumber().length() != 16) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage("Wrong credit card number",
                            "The credit card number is wrong"));
            return "validationError";
        } else {
            card.setValidity(creditCardValidity[0] + creditCardValidity[1]);
            card.setOwnerName(contract.getCustomer().getName());
            contract.getCustomer().setCard(card);

            manager.saveContractNewCustomer(contract, contract.getHoliday()
                    .getId());
        }

        this.contract = null;
        this.creditCardNo = new String[4];
        this.creditCardValidity = new String[2];
        this.currentTraveller = null;

        return "confirmed";
    }

}
